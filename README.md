![](./logo.png)

**SIMON** is a game for the [flow3r](https://flow3r.garden/), the official badge of the [CCCamp2023](https://events.ccc.de/camp/2023/infos/).

The gameplay follows the one of the original [Simon](https://en.wikipedia.org/wiki/Simon_(game)). You play using the five white petals of the flow3r as buttons.

## Pictures

![The menu](./example.jpg)