from st3m import logging
from st3m.application import Application, ApplicationContext
from st3m.input import InputController, InputState
from st3m.ui import colours
from st3m.ui.view import ViewManager
from st3m.goose import Optional, Generator
from ctx import Context
from .led_segments import light_part, dark_part
import bl00mbox
import random
import st3m.run
import leds
import math

def guesses_in_round(round_nb):
    return round_nb + 2

class Simon(Application):
    log = logging.Log(__name__, level=logging.INFO)

    model = []

    input: InputController
    channel: bl00mbox.Channel
    samples: list[bl00mbox.sampler] = []
    
    scene = 'menu' # 'menu' | 'start' | 'show' | 'ask' | 'next' | 'end'
    round_nb = 0

    menu_count = 0

    start_countdown = 120

    show_guess_number = 0
    show_guess_countdown = 0

    ask_guess_number = 0
    ask_current_pressed = 0

    next_countdown = 60

    end_countdown = 180

    sample_names: list[str] = [
        "kick.wav",
        "snare.wav",
        "hihat.wav",
        "crash.wav",
        "close.wav"
    ]
    is_loading: bool = True

    def __init__(self, app_ctx: ApplicationContext):
        super().__init__(app_ctx)
        self.input = InputController()

        self.channel = bl00mbox.Channel("Simple Drums")
        self.reset_game()

    def reset_game(self):
        self.scene = 'menu'
        self.start_countdown = 120
        self.show_guess_number = 0
        self.show_guess_countdown = 0
        self.ask_guess_number = 0
        self.ask_current_pressed = 0
        self.end_countdown = 240
        self.round_nb = 0
        self.model = []
        for i in range(200):
            self.model.append(random.randrange(1, 10, 2))

    def play_sound(self, sound_nb):
        if (sound_nb == 1):
            self.samples[0].signals.trigger.start()
        if (sound_nb == 3):
            self.samples[1].signals.trigger.start()
        if (sound_nb == 5):
            self.samples[2].signals.trigger.start()
        if (sound_nb == 7):
            self.samples[3].signals.trigger.start()
        if (sound_nb == 9):
            self.samples[4].signals.trigger.start()

    def press(self, touch):
        if self.ask_current_pressed == 0:
            light_part(touch)
            self.ask_current_pressed = touch

    def release(self, touch):
        if self.ask_current_pressed == touch:
            self.ask_current_pressed = 0
            dark_part(touch)

    def scene_menu(self):
        self.menu_count += 1

        phase = self.menu_count % 360

        if phase > 0 and phase < 60:
            light_part(1)
        if phase > 60 and phase < 120:
            light_part(3)
            dark_part(1)
        if phase > 120 and phase < 180:
            light_part(5)
            dark_part(3)
        if phase > 180 and phase < 240:
            light_part(7)
            dark_part(5)
        if phase > 240 and phase < 300:
            light_part(9)
            dark_part(7)
        if phase > 300 and phase < 360:
            dark_part(9)

        if self.input.buttons.app.middle.pressed:
            dark_part(1)
            dark_part(3)
            dark_part(5)
            dark_part(7)
            dark_part(9)
            self.scene = 'start'

    def scene_next(self):
        self.next_countdown -= 1
        if self.next_countdown == 0:
            self.scene = 'show'

    def scene_start(self):
        self.start_countdown -= 1
        if self.start_countdown == 0:
            self.show_guess_number = 0
            self.show_guess_countdown = 60
            self.scene = 'show'

    def scene_show(self):
        time_to_show = max(60 - self.round_nb * 2, 40)

        self.show_guess_countdown -= 1
        if self.show_guess_countdown == 0:
            # If it was the last blink, next scene
            if (self.show_guess_number + 1 == guesses_in_round(self.round_nb)):
                self.scene = 'ask'
                return
            # else, next blink
            self.show_guess_countdown = 60
            self.show_guess_number += 1

        if self.show_guess_countdown == time_to_show - 5:
            light_part(self.model[self.show_guess_number])
            self.play_sound(self.model[self.show_guess_number])
        if self.show_guess_countdown == 5:
            dark_part(self.model[self.show_guess_number])

    def scene_ask(self):
        if self.ask_guess_number == guesses_in_round(self.round_nb):
            self.ask_guess_number = 0
            self.show_guess_number = 0
            self.show_guess_countdown = 60
            self.round_nb = self.round_nb + 1
            self.next_countdown = 90
            self.scene = 'next'
            return

        if self.ask_current_pressed == 0:
            if self.input.captouch.petals[1].pressure > 0:
                self.press(1)
                self.play_sound(1)
            elif self.input.captouch.petals[3].pressure > 0:
                self.press(3)
                self.play_sound(3)
            elif self.input.captouch.petals[5].pressure > 0:
                self.press(5)
                self.play_sound(5)
            elif self.input.captouch.petals[7].pressure > 0:
                self.press(7)
                self.play_sound(7)
            elif self.input.captouch.petals[9].pressure > 0:
                self.press(9)
                self.play_sound(9)
        else:
            if self.ask_current_pressed == 1 and not self.input.captouch.petals[1].pressure > 0:
                self.release(1)
                self.verify_or_gameover(1)
            elif self.ask_current_pressed == 3 and not self.input.captouch.petals[3].pressure > 0:
                self.release(3)
                self.verify_or_gameover(3)
            elif self.ask_current_pressed == 5 and not self.input.captouch.petals[5].pressure > 0:
                self.release(5)
                self.verify_or_gameover(5)
            elif self.ask_current_pressed == 7 and not self.input.captouch.petals[7].pressure > 0:
                self.release(7)
                self.verify_or_gameover(7)
            elif self.ask_current_pressed == 9 and not self.input.captouch.petals[9].pressure > 0:
                self.release(9)
                self.verify_or_gameover(9)

    def verify_or_gameover(self, current):
        if self.model[self.ask_guess_number] == current:
            self.ask_guess_number += 1
        else:
            self.scene = "end"
            self.end_countdown = 240

    def scene_end(self):
        self.end_countdown -= 1
        if self.end_countdown == 0:
            self.reset_game()

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)
        if self.is_loading:
            self._load_next_sample()
            return

        if self.scene == 'start':
            self.scene_start()
        elif self.scene == 'menu':
            self.scene_menu()
        elif self.scene == 'show':
            self.scene_show()
        elif self.scene == 'ask':
            self.scene_ask()
        elif self.scene == 'next':
            self.scene_next()
        elif self.scene == 'end':
            self.scene_end()

    def draw(self, ctx: Context) -> None:
        ctx.rgb(*colours.BLACK)
        ctx.rectangle(
            -120.0,
            -120.0,
            240.0,
            240.0,
        ).fill()
        ctx.font = "Camp Font 1"

        if self.is_loading:
            if len(self.samples) != len(self.sample_names):
                ctx.rgb(*colours.GO_GREEN)
                ctx.text_baseline = ctx.MIDDLE
                ctx.text_align = ctx.CENTER
                ctx.move_to(0, 0)
                ctx.font_size = 20
                ctx.font = "Camp Font 1"
                ctx.text(f"Loading sample {len(self.samples) + 1}/{len(self.sample_names)}")
                print(f"Loading sample {len(self.samples) + 1}/{len(self.sample_names)}")

                progress = len(self.samples) / len(self.sample_names)

                ctx.rectangle(-80, 40, 160, 20)
                ctx.rgb(1,1,1)
                ctx.fill()
                ctx.rectangle(-79, 41, 158, 18)
                ctx.rgb(0,0,0)
                ctx.fill()

                ctx.rectangle(-79, 41, int(158 * progress), 18)
                ctx.rgb(0,1,0)
                ctx.fill()
        else:
            if self.scene == 'menu':
                ctx.text_baseline = ctx.MIDDLE
                ctx.text_align = ctx.CENTER
                ctx.font = "Camp Font 1"

                ctx.image("/flash/sys/apps/simon/bg.png", -120, -120, 240, 240)
                ctx.font_size = 60
                ctx.rgba(0, 0, 0, 0.8 + math.sin(self.menu_count / 80.0) * 0.2)
                ctx.rectangle(
                    -120.0,
                    -120.0,
                    240.0,
                    240.0,
                ).fill()
                ctx.rgb(*colours.PUSH_RED)
                ctx.move_to(0, 0)
                ctx.text("SIMON")
                if math.sin(self.menu_count / 30.0) >= 0:
                    ctx.move_to(0, 40)
                    ctx.font_size = 20
                    ctx.rgb(*colours.GO_GREEN)
                    ctx.text("Press left trigger")

                ctx.font_size = 15
                ctx.rgba(0, 1, 0, 0.4)
                ctx.move_to(0, 110)
                ctx.text("v2")

            if self.scene == 'start':
                ctx.rgb(*colours.GO_GREEN)
                ctx.text_baseline = ctx.MIDDLE
                ctx.text_align = ctx.CENTER
                ctx.move_to(0, 0)
                ctx.font_size = 30
                ctx.font = "Camp Font 1"

                ctx.text(f"Starting in {int(self.start_countdown / 60) + 1}...")
                
            if self.scene == 'show':
                ctx.rgb(*colours.GO_GREEN)
                ctx.text_baseline = ctx.MIDDLE
                ctx.text_align = ctx.CENTER
                ctx.move_to(0, 0)
                ctx.font = "Camp Font 1"

                ctx.font_size = 40
                ctx.text(f"Round {self.round_nb}")
                
            if self.scene == 'ask':
                if self.ask_current_pressed == 1:
                    ctx.image("/flash/sys/apps/simon/tap_1.png", -120, -120, 240, 240)
                elif self.ask_current_pressed == 3:
                    ctx.image("/flash/sys/apps/simon/tap_3.png", -120, -120, 240, 240)
                elif self.ask_current_pressed == 5:
                    ctx.image("/flash/sys/apps/simon/tap_5.png", -120, -120, 240, 240)
                elif self.ask_current_pressed == 7:
                    ctx.image("/flash/sys/apps/simon/tap_7.png", -120, -120, 240, 240)
                elif self.ask_current_pressed == 9:
                    ctx.image("/flash/sys/apps/simon/tap_9.png", -120, -120, 240, 240)

                ctx.text_baseline = ctx.MIDDLE
                ctx.text_align = ctx.CENTER
                ctx.move_to(0, 0)
                ctx.font_size = 30
                ctx.font = "Camp Font 1"
                ctx.rgb(*colours.WHITE)
                ctx.text("Your turn")

            if self.scene == 'next':
                ctx.text_baseline = ctx.MIDDLE
                ctx.text_align = ctx.CENTER
                ctx.move_to(0, 0)

                c = max(0, (self.next_countdown - 20) / 60.0)

                ctx.rgb(0,0,0)
                ctx.rectangle(
                    -120.0,
                    -120.0,
                    240.0,
                    240.0,
                ).fill()
                ctx.rgb(c,c,c)
                ctx.font_size = 40
                ctx.text("Correct")
                ctx.move_to(0, 60)

            if self.scene == 'end':
                ctx.text_baseline = ctx.MIDDLE
                ctx.text_align = ctx.CENTER
                ctx.move_to(0, 0)

                ctx.rgb(*colours.RED)
                ctx.rectangle(
                    -120.0,
                    -120.0,
                    240.0,
                    240.0,
                ).fill()
                ctx.rgb(*colours.BLACK)
                ctx.font_size = 40
                ctx.text("Game over")
                ctx.move_to(0, 60)
                ctx.font_size = 20
                ctx.text("Score : ")
                ctx.move_to(0, 90)
                ctx.font_size = 40
                ctx.text(f"{self.round_nb}")

        ctx.restore()

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        super().on_enter(vm)

    def verify(self, next_guess):
        print(f"Just guessed {next_guess}")
        print(f"It was {self.model[self.current_guess]}")
        if self.model[self.current_guess] == next_guess:
            return True
        else:
            return False

    def _load_next_sample(self) -> None:
        loaded = len(self.samples)
        if loaded >= len(self.sample_names):
            self.is_loading = False
            return

        i = loaded

        sample = self.channel.new(bl00mbox.patches.sampler, self.sample_names[i - 1])
        sample.signals.output = self.channel.mixer

        self.samples.append(sample)

if __name__ == '__main__':
    st3m.run.run_view(Simon(ApplicationContext()))
