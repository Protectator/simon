import leds

hues = [0, 50, 100, 180, 230]

def light_part(part):
    segment = int((part - 1) / 2)
    leds.set_hsv(segment * 8 + 0, hues[segment], 1, 1)
    leds.set_hsv(segment * 8 + 1, hues[segment], 1, 1)
    leds.set_hsv(segment * 8 + 2, hues[segment], 1, 1)
    leds.set_hsv(segment * 8 + 3, hues[segment], 1, 1)
    leds.set_hsv(segment * 8 + 4, hues[segment], 1, 1)
    leds.set_hsv(segment * 8 + 5, hues[segment], 1, 1)
    leds.set_hsv(segment * 8 + 6, hues[segment], 1, 1)
    leds.set_hsv(segment * 8 + 7, hues[segment], 1, 1)
    leds.update()

def dark_part(part):
    segment = int((part - 1) / 2)
    leds.set_hsv(segment * 8 + 0, hues[segment], 1, 0)
    leds.set_hsv(segment * 8 + 1, hues[segment], 1, 0)
    leds.set_hsv(segment * 8 + 2, hues[segment], 1, 0)
    leds.set_hsv(segment * 8 + 3, hues[segment], 1, 0)
    leds.set_hsv(segment * 8 + 4, hues[segment], 1, 0)
    leds.set_hsv(segment * 8 + 5, hues[segment], 1, 0)
    leds.set_hsv(segment * 8 + 6, hues[segment], 1, 0)
    leds.set_hsv(segment * 8 + 7, hues[segment], 1, 0)
    leds.update()
